This directory is appended to the `$PATH` variable. Files placed in here will be callable by name in productions that include this package.
