This directory is appended to the `$PYTHONPATH` variable. Files placed in here will be `import`able by name in other python scripts (e.g. in the `/options` folder.
