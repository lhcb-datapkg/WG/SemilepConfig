"""
Stripping filter options for Bc->D0munu 2015 MC (S24r2) with no PID requirements
@author Alison Tully
@date 2020-07-28
"""

#use CommonParticlesArchive
stripping='stripping24r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingSettings.Stripping24r2.LineConfigDictionaries_Semileptonic import B2DMuNuX
from StrippingSelections.StrippingSL import StrippingB2DMuNuX
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration

B2DMuNuX['CONFIG']['UseNoPIDsInputs'] = True
B2DMuNuX['CONFIG']['MuonPIDmu']       = -99999
B2DMuNuX['CONFIG']['KaonPIDK']        = -99999
B2DMuNuX['CONFIG']['PionPIDK']        =  99999

MyStream = StrippingStream("Bc2D0MuNu.Strip")
confB2DMuNuX = StrippingB2DMuNuX.B2DMuNuXAllLinesConf("B2DMuNuX", B2DMuNuX['CONFIG'])
lineList= confB2DMuNuX.lines()

MyLines = []
for l in lineList:
    if 'StrippingB2DMuNuX_D0' in l.name():
        MyLines.append(l)
MyStream.appendLines(MyLines)

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip',
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck() )

MyStream.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105242)


# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType = "2015"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

