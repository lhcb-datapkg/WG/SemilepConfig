#!/usr/bin/env gaudirun.py
# 
# Melody Ravonel Salzgeber
# Filter on various stripping line:
# -B2DMuNuX_Dp
# -B2DMuNuX_Ds
# -b2DsMuXB2DMuForTauMuLine
# 2017
# 
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.Configuration import StrippingConf

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from StrippingSettings.Stripping24.LineConfigDictionaries_Semileptonic import B2DMuNuX, B2DMuForTauMu
from StrippingSelections.StrippingSL import StrippingB2DMuNuX, StrippingB2DMuNuXUtils, StrippingB2DMuForTauMu
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdLooseElectrons
from StandardParticles import StdNoPIDsPions, StdNoPIDsKaons,StdNoPIDsProtons,StdNoPIDsMuons
from GaudiKernel.SystemOfUnits import MeV, GeV, cm, mm


# Substitute the b2munux line PID cuts.
B2DMuNuX['CONFIG']['UseNoPIDsInputs']= True
B2DMuNuX['CONFIG']['MuonPIDmu']      = -99999
B2DMuNuX['CONFIG']['ProtonPIDp']     = -99999
B2DMuNuX['CONFIG']['ProtonPIDpK']    = -99999
B2DMuNuX['CONFIG']['KaonPIDK']       = -99999
B2DMuNuX['CONFIG']['PionPIDK']       = 99999
B2DMuNuX['CONFIG']['ElectronPIDe']   = -99999
B2DMuNuX['CONFIG']['TTSpecs']        = {};

# Substitute the tauonic line PID cuts.
B2DMuForTauMu['CONFIG']['PIDmu']         = -99999
B2DMuForTauMu['CONFIG']['KaonPIDK']      = -99999
B2DMuForTauMu['CONFIG']['PionPIDKTight'] =  99999
B2DMuForTauMu['CONFIG']['FakePrescale'] =  1.0
B2DMuForTauMu['CONFIG']['M_MAX'] =  2010.0
B2DMuForTauMu['CONFIG']['M_MIN'] =  1920.0

MyStream = StrippingStream("B2DMuNuXANDTAU")
#confB2DMuNuX      = StrippingB2DMuNuX.B2DMuNuXAllLinesConf("B2DMuNuX", MYCONFIG)
confB2DMuNuX = StrippingB2DMuNuX.B2DMuNuXAllLinesConf("B2DMuNuX", B2DMuNuX['CONFIG'])
lineList= confB2DMuNuX.lines()
for l in lineList:
    MyLines = []
    if 'B2DMuNuX_Dp'  in l.name():
        MyLines.append(l)
    if 'B2DMuNuX_Ds' in l.name():
        MyLines.append(l)
    MyStream.appendLines( MyLines )

confB2DMuForTauMu = StrippingB2DMuForTauMu.B2DMuForTauMuconf("B2DMuForTauMu", B2DMuForTauMu['CONFIG'])
lineList2= confB2DMuForTauMu.lines()
replaceDictFilterDesktop = { 'MuforB2DMuForTauMu'  : 'Phys/StdAllNoPIDsMuons/Particles'
                             ,'PiforB2DMuForTauMu' : 'Phys/StdAllNoPIDsPions/Particles'
                             ,'KforB2DMuForTauMu'  : 'Phys/StdAllNoPIDsKaons/Particles' }
replaceDictVoidFilter = { 'SelFilterPhys_StdAllLooseMuons_Particles' : "\n            0<CONTAINS('Phys/StdAllNoPIDsMuons/Particles',True)\n            "
                          ,'SelFilterPhys_StdLooseKaons_Particles'   : "\n            0<CONTAINS('Phys/StdAllNoPIDsKaons/Particles',True)\n            "
                          ,'SelFilterPhys_StdLoosePions_Particles'   : "\n            0<CONTAINS('Phys/StdAllNoPIDsPions/Particles',True)\n            " }
for l in lineList2:
    MyLines = []
    # Get the line and change in place the inputs/code in the sequences.
    if 'b2DsMuXB2DMuForTauMuLine' in l.name():
        for i in range(len(l._members)):
            seq = l._members[i]
            # Replace the inputs in the FilterDesktop sequences.
            if seq.name() in replaceDictFilterDesktop.keys():
                seq.Inputs = [ replaceDictFilterDesktop[seq.name()] ]
                l._members = l._members[:i] + [seq] + l._members[i+1:]
            # Hack the Code attribute in the void filters.
            if seq.name() in replaceDictVoidFilter.keys():
                seq.Code = replaceDictVoidFilter[seq.name()]
                l._members = l._members[:i] + [seq] + l._members[i+1:]
        MyLines.append(l)
    MyStream.appendLines( MyLines )
    
locations = []
for lin in MyStream.lines:
    print lin.outputLocation()
    locations.append(lin.outputLocation())

dstStreams  = [ "Semileptonic"]
stripTESPrefix = 'Strip'
from Configurables import ProcStatusCheck
sc = StrippingConf( Streams = [MyStream] ,
                    MaxCandidates = 20000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    Verbose = True,
                    DSTStreams = dstStreams)

# So that we do not get all events written out
MyStream.sequence().IgnoreFilterPassed = False

# Configure the dst writers for the output
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }
SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          OutputFileSuffix ='noPIDHackNoCuts',
                          SelectionSequences = sc.activeStreams() )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41222600)
##0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number


##################################################################################################
#Stripping reportes provide the following information:
#Selection results in each event
#Summary of selection results (number of selected events, accept rate and average multiplicity) every N events.
#Summary of selection results in the end of the job.
#List of noisy selections (with the accept rate above certain threshold) and non-responding selections (that select zero events)
from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections(),ReportFrequency = 5000)
# ChronoAuditor is used by StrippingReport to show the timing.
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )
##################################################################################################


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

## trigger filters :
#from PhysConf.Filters import LoKi_Filters
#fltrs = LoKi_Filters (
#    L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon')",
#    HLT1_Code = "HLT_PASS_RE('Hlt1Track.*Decision')  | HLT_PASS_RE('Hlt1.*Muon.*Decision')" , 
#    HLT2_Code = "HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2.*SingleMuon.*Decision')" 
#    )
#DaVinci().EventPreFilters = fltrs.filters('PhysTrigFilters')

DaVinci().appendToMainSequence( [ eventNodeKiller,sc.sequence(),stck,dstWriter.sequence() ] )
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000


DaVinci().ProductionType = "Stripping"
DaVinci().DataType  = "2015"
DaVinci().InputType = "DST"

DaVinci().Simulation = True
DaVinci().Lumi =  not DaVinci().Simulation


# database
DaVinci().DDDBtag   = "dddb-20150724"
DaVinci().CondDBtag = "sim-20160606-vc-mu100"

# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
