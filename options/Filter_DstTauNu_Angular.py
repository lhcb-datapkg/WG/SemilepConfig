# $Id: $
# Test your line(s) of the stripping
#
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf
import sys, os

###
# Need to know the SL environment variable
sys.path.append(os.environ["SEMILEPCONFIGOPTS"])
###
from Filter_DstTauNu_Mass_FlightSig_Stripping import default_config, B2XTauNuAllLinesConf

from StrippingConf.Configuration import StrippingConf, StrippingStream
'''
#select only signal line
default_config['CONFIG']['Prescale_B0d2DstarTauNu'] = 1.0
default_config['CONFIG']['Prescale_B0d2DstarWSSlowTauNu'] = 0.
default_config['CONFIG']['Prescale_B0d2DTauNu']  = 0.
default_config['CONFIG']['Prescale_Bu2D0TauNu']  = 0.
default_config['CONFIG']['Prescale_B0s2DsTauNu']   = 0.
default_config['CONFIG']['Prescale_Bc2JpsiTauNu']  = 0.
default_config['CONFIG']['Prescale_Lb2LcTauNu'] = 0.
default_config['CONFIG']['Prescale_Lb2pTauNu']  = 0.
default_config['CONFIG']['Prescale_Bu2DdoubleStar0TauNu'] = 0.
default_config['CONFIG']['Prescale_Bu2Dstar02Pi0D0TauNu'] = 0.
default_config['CONFIG']['Prescale_Bu2Dstar02GammaD0TauNu']  = 0.
default_config['CONFIG']['Prescale_B0d2DdoubleStar2PiDstar02Pi0D0TauNu'] = 0.
default_config['CONFIG']['Prescale_B0d2DdoubleStar2PiDstar02GammaD0TauNu']  = 0.
default_config['CONFIG']['Prescale_NonPhys'] = 0.
'''

builder = B2XTauNuAllLinesConf(default_config['NAME'],default_config['CONFIG'])
# mystreamname = "D02MUMU"
mystreamname = "B0d2DstarTauNu_MassFlightCut"

stream = StrippingStream(mystreamname)
stream.appendLines( builder.lines() )

mdstStreams = []
dstStreams = [mystreamname]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()


sc = StrippingConf( Streams = [stream],
                    MaxCandidates = 2000,
                    TESPrefix = stripTESPrefix,
                    DSTStreams = dstStreams,
                    AcceptBadEvents=False,
                    BadEventSelection=filterBadEvents
                    )

## we get only filtered events
stream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out
#
# Configure the dst writers for the output
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
dstStreamConf = stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False,isMC=True)
dstElements   = stripDSTElements(pack=enablePacking,isMC=True)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    mystreamname            : dstElements
    }

SelDSTWriterConf = {
    mystreamname             : dstStreamConf,
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf[mystreamname].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )


# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().PrintFreq = 10000
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

