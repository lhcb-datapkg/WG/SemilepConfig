"""
Stripping filtering file for Lb->LcXc MC cocktails where Xc = Ds, D-, D0
@author Victor Renaudin
@date   2017-10-05
"""
# Stripping version
stripping = 'stripping28'

# Use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts = {
    "Chi2Cut":[0, 3],
    "CloneDistCut":[5000, 9e+99]}

# Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

# Get the configuration dictionary from the database
config = strippingConfiguration(stripping)
# Get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping=config, archive=archive)

# Select my lines
MyStream = StrippingStream("Lb2LcXc.StripTrig")
MyLines = [
    'StrippingLb2LcTauNuForB2XTauNuAllLines',
    'StrippingLb2LcTauNuInvVertForB2XTauNuAllLines',
    'StrippingLb2LcTauNuWSForB2XTauNuAllLines',
    'StrippingLb2LcTauNuNonPhysTauForB2XTauNuAllLines',
]
# Remove all prescalings
# Merge into one stream and run in flag mode

LinesToAdd = []
for stream in streams:
    for line in stream.lines:
        line._prescale = 1.0
        for MyLine in MyLines:
            if line.name() == MyLine:
                MyStream.appendLines([line])

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

# Add trigger requirement
from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci
trigger_selection = LoKi_Filters(
    L0DU_Code="(L0_CHANNEL_RE('Global') | L0_CHANNEL('Hadron'))",
    HLT1_Code="(HLT_PASS_RE('Hlt1TwoTrack.*Decision') | \
    HLT_PASS_RE('Hlt1Track.*Decision'))",
    HLT2_Code="(HLT_PASS_RE('Hlt2.*Topo.*Decision') | \
    HLT_PASS_RE('Hlt2CharmHad.*Decision') | HLT_PASS_RE('Hlt2XcMu.*Decision'))",
)
DaVinci().EventPreFilters = trigger_selection.filters('Trigger_Selection')

sc = StrippingConf(Streams=[MyStream],
                   MaxCandidates=2000,
                   TESPrefix='Strip',
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents
                   )

# So that we do not get all events written out
MyStream.sequence().IgnoreFilterPassed = False
# Configuration of SelDSTWriter

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False)}

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='000000',
                         SelectionSequences=sc.activeStreams()
                         )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports', TCK=0x42302800)

# DaVinci Configuration
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
