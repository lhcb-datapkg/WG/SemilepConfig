"""
Stripping filtering file for Monte Carlo for B2DXMuNu for b-fractions analysis
2016 data
@author S. Ely
@date   2017-11-06
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingSelections.StrippingSL import StrippingB2DMuNuX, StrippingB2DMuNuXUtils
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive
import shelve

from StrippingSettings.Stripping28.LineConfigDictionaries_Semileptonic import B2DMuNuX
from StrippingSelections.StrippingSL import StrippingB2DMuNuX, StrippingB2DMuNuXUtils
from StrippingSelections.StrippingSL.StrippingB2DMuNuX import B2DMuNuXAllLinesConf
from StrippingSelections.StrippingSL.StrippingB2DMuNuX import default_config as configs

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = ['/Event/AllStreams','/Event/Strip']

#stripping version
stripping='stripping28r1'

# update configs to remove PID info
configs['B2DMuNuX']['CONFIG']['UseNoPIDsInputs'] = True
configs['B2DMuNuX']['CONFIG']['MuonPIDmu'] = -1000.
configs['B2DMuNuX']['CONFIG']['ProtonPIDp'] = -1000.
configs['B2DMuNuX']['CONFIG']['ProtonPIDpK'] = -1000.
configs['B2DMuNuX']['CONFIG']['KaonPIDK'] = -1000.
configs['B2DMuNuX']['CONFIG']['PionPIDK'] = 1000.
configs['B2DMuNuX']['CONFIG']['ElectronPIDe'] = -1000.
configs['B2DMuNuX']['CONFIG']['HLT2'] = "HLT_PASS_RE('Hlt2.*Decision')"
configs['B2DMuNuX']['CONFIG']['TTSpecs'] = {}


confB2DMuNuX = B2DMuNuXAllLinesConf(name='B2DMuNuX',config = configs['B2DMuNuX']['CONFIG'])

streams = configs['B2DMuNuX']['STREAMS']['Semileptonic']
lines = []
for x in streams:
    lines.append(confB2DMuNuX.lineFromName(x))

#get the line builders from the archive
archive = strippingArchive(stripping)

for l in lines:
    print "added line "+l.name()

MyStream = StrippingStream("b2DMuNuXStream.Strip")
MyLines = []
#for stream in streams:
for line in lines:
    line._prescale = 1.0
    MyLines.append(line)
MyStream.appendLines(MyLines)

#
# Remove All prescalings
# Merge into one stream and run in flag mode
#

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'
                    )

MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())
sr.OnlyPositive = False

#
# Configuration of SelDSTWriter
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports',TCK=0x41442810)

#
# DaVinci Configuration
#
from Configurables import DaVinci

dv = DaVinci()

dv.InputType = 'DST'
dv.DataType = "2016"
dv.EvtMax = -1
dv.Simulation = True
dv.Lumi = False
#dv.DDDBtag   = "dddb-20150724"
#dv.CondDBtag = "sim-20160907-vc-md100"
dv.HistogramFile = "DVHistos.root"
dv.appendToMainSequence([eventNodeKiller])
dv.appendToMainSequence([ sc.sequence() ])
dv.appendToMainSequence([sr])
dv.appendToMainSequence([ stck ])
dv.appendToMainSequence([ dstWriter.sequence() ])
dv.ProductionType = "Stripping"

# Change the column size of Timing Table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
