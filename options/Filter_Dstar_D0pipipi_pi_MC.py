"""
Options for building Stripping17 with strict ordering
of streams for MC.
"""

from Gaudi.Configuration import *

# Rest is stripping

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object - bring in some necessary modules
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)          # Goes away and gets the relevant stripping cut dictionaries from Phys/StrippingSettings
#get the line builders from the archive
archive = strippingArchive(stripping)       # Goes away and gets the relevant line builders from Phys/StrippingSelections

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []        

#Select the streams you are interested in - in this case just the charm complete event and MDST streams 

_charm_complete = quickBuild('CharmCompleteEvent')

_charm_complete.lines[:] = [ x for x in _charm_complete.lines if 'DstRSwD02K2Pi' in x.name() ]
for line in _charm_complete.lines :
    print "charm complete has a line called " + line.name()



streams.append( _charm_complete )

#
#----------------------------------- turn off all pre-scalings 
#
## for stream in streams:                  ## Prescaling 
##     for line in stream.lines:
##         line._prescale = 1.0 

#---------------------------------------------------------------

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("DstD02KnPi.Strip")

for stream in streams:                       # stream = "member" of streams
    AllStreams.appendLines(stream.lines)          # add all the lines associated with each of the streams

    
sc = StrippingConf( Streams = [ AllStreams ],
                    HDRLocation="Phys/DecReports",
                    MaxCandidates = 2000 )        # Max candidates

AllStreams.sequence().IgnoreFilterPassed = False # we wish to run the stripping in rejection mode



from Configurables import DaVinci
########################################################################
DaVinci().UserAlgorithms += [ sc.sequence() ]





# Import the MDST and DST writers

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )



#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }


for stream in sc.activeStreams() :
    print "there is a stream called " + stream.name() + " active"
    


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )







#-------------------------------------
#
#------------------------- DaVinci Configuration
#
from Configurables import DaVinci


DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
