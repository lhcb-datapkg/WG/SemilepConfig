"""
Stripping filtering file for Bd->D*Ds(3piN) Monte Carlo
@author Guy Wormser
@date   2014-07-28
"""


from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
#from CommonParticles.Utils import DefaultTrackingCuts
#DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
#                               "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

stream = buildStream(stripping = config, streamName='Semilep', archive = archive)
# Select my lines
MyStream = StrippingStream("B0DstarDs3piN.TrigStrip")
MyLines = [ 'Bd2DstarTauNuLine','Bd2DstarTauNuWSLine' ]
#
# Remove all prescalings 
# Merge into one stream and run in flag mode
#
for line in stream.lines:
        # Set prescales to 1.0 if necessary
            line._prescale = 1.0
            if line.name() in MyLines :
                        MyStream.appendLines( [ line ] )

                      

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )
#add trigger requirement
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon') | L0_CHANNEL('Hadron')",
        HLT_Code = "(HLT_PASS_RE('Hlt1Track.*Decision') | HLT_PASS_RE('Hlt1.*Muon.*Decision')) & (HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2.*Muon.*Decision'))"
            )
from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32220201)

#
# DaVinci Configuration
#
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                     # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

