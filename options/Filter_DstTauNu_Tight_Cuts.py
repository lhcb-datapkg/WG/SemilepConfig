# $Id: $
# Test your line(s) of the stripping
#
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#
# To Test:
# lb-run DaVinci/v36r1p1 bash --norc
# gaudirun.py Conditions.py /cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r270/options/DaVinci/DataType-2012.py /cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r270/options/DaVinci/InputType-DST.py LoadDSTs.py Filter_DstTauNu_Tight_Cuts.py

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf
import sys, os

###
# Need to know the SL environment variable
sys.path.append(os.environ["SEMILEPCONFIGOPTS"])
#sys.path.append('/afs/cern.ch/user/l/lscantle/b2clv_angular_analysis/filtering')
###
from Filter_DstTauNu_Tight_Cuts_Stripping import default_config, B2XTauNuAllLinesConf

from StrippingConf.Configuration import StrippingConf, StrippingStream

#select only signal line
default_config['CONFIG']['Prescale_B0d2DstarTauNuTightCuts'] = 1.0
default_config['CONFIG']['Prescale_B0d2DstarTauNu'] = 0.
default_config['CONFIG']['Prescale_B0d2DstarWSSlowTauNu'] = 0.
default_config['CONFIG']['Prescale_B0d2DTauNu']  = 0.
default_config['CONFIG']['Prescale_Bu2D0TauNu']  = 0.
default_config['CONFIG']['Prescale_B0s2DsTauNu']   = 0.
default_config['CONFIG']['Prescale_Bc2JpsiTauNu']  = 0.
default_config['CONFIG']['Prescale_Lb2LcTauNu'] = 0.
default_config['CONFIG']['Prescale_Lb2pTauNu']  = 0.
default_config['CONFIG']['Prescale_Bu2DdoubleStar0TauNu'] = 0.
default_config['CONFIG']['Prescale_Bu2Dstar02Pi0D0TauNu'] = 0.
default_config['CONFIG']['Prescale_Bu2Dstar02GammaD0TauNu']  = 0.
default_config['CONFIG']['Prescale_B0d2DdoubleStar2PiDstar02Pi0D0TauNu'] = 0.
default_config['CONFIG']['Prescale_B0d2DdoubleStar2PiDstar02GammaD0TauNu']  = 0.
default_config['CONFIG']['Prescale_NonPhys'] = 0.

builder = B2XTauNuAllLinesConf(default_config['NAME'], default_config['CONFIG'])
mystreamname = "RXcHad.Strip"
stream = StrippingStream(mystreamname)
stream.appendLines( builder.lines() )

mdstStreams = []
dstStreams = [mystreamname]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(
    Streams=[stream],
    MaxCandidates=2000,
    TESPrefix=stripTESPrefix,
    DSTStreams=dstStreams,
    AcceptBadEvents=False,
    BadEventSelection=filterBadEvents
)

## we get only filtered events
stream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out
#
# Configure the dst writers for the output
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf, stripDSTElements)

#
# Configuration of DST
# per-event and per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
#dstStreamConf = stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False, isMC=True)
#dstElements   = stripDSTElements(pack=enablePacking, isMC=True)
dstStreamConf = stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False)
dstElements   = stripDSTElements(pack=enablePacking)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {mystreamname : dstElements}
SelDSTWriterConf = {mystreamname : dstStreamConf}

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf[mystreamname].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='000000',
    SelectionSequences=sc.activeStreams()
)


# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36152100)

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60

