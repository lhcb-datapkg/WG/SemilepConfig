
# Filtering file for Dstar->D0(Kpi)pi with StrippingNoPIDDstarLine removing cuts to D0 mass and prompt D0
# Alejandro Rodriguez
# 2018



from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
MessageSvc().OutputLevel = INFO


# Check for PVs
from Configurables import CheckPV
checkPV = CheckPV('CheckPVMin1')
checkPV.MinPVs = 1


#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream, StrippingLine
from StrippingSettings.Utils import strippingConfiguration
from GaudiKernel.SystemOfUnits import MeV, GeV, cm, mm



# Create the new stream and define the new line's name
MyStream = StrippingStream("Dstar2D02KpiNoPID")
name = 'noPIDDstar2D0pi'



# Define the new lines cuts
from Configurables import CombineParticles,  FilterDesktop
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence

lkpreambulo=["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch", "from LoKiHlt.algorithms import *"]

cpD02Kpi = CombineParticles("cpD0P")
cpD02Kpi.Preambulo = lkpreambulo 

cpD02Kpi.DecayDescriptors = ["[D0 -> K- pi+]cc"]
cpD02Kpi.DaughtersCuts = { "": "ALL",
                            "K-"  :"(P > 2000.0) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
                           "pi+" :"(P > 2000.0) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
                           "K+"  :"(P > 2000.0) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)",
                           "pi-" :"(P > 2000.0) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 16)"
                           }
cpD02Kpi.MotherCut = "(PT>1500.0) & (VFASPF(VCHI2PDOF)<13) & ( ADWM( 'D0' , WM( 'pi-' , 'K+') ) > 25.0)"

selcpD02Kpi = Selection("selcpD02Kpi", Algorithm = cpD02Kpi, RequiredSelections = [DataOnDemand("Phys/StdAllNoPIDsKaons/Particles"), 
                                                                                   DataOnDemand("Phys/StdAllNoPIDsPions/Particles")])
selseqcpD02Kpi = SelectionSequence("selseqcpD02Kpi",TopSelection=selcpD02Kpi)

  
cpDst = CombineParticles("cpDst")
# cpDst.Preambulo = lkpreambulo
cpDst.Preambulo = lkpreambulo

cpDst.DecayDescriptors = ["[D*(2010)+ -> D0 pi+]cc"]
cpDst.DaughtersCuts = { "": "ALL",
                        "pi+" : "(PT>150.0) & (TRCHI2DOF < 5)",
                        "D~0" : "ALL",
                        "D0" : "ALL",
                        "pi-" : "(PT>150.0) & (TRCHI2DOF < 5)"
                        }
cpDst.CombinationCut = "(APT>2200.0) & (AM - AM1 < 165 * MeV)"
cpDst.MotherCut = "(VFASPF(VCHI2PDOF)<13) & (M-MAXTREE('D0'==ABSID,M)<155.0) & (M-MAXTREE('D0'==ABSID,M)>130.0)"


selcpDst = Selection( "selcpDst", Algorithm = cpDst, RequiredSelections = [selcpD02Kpi, DataOnDemand("Phys/StdAllNoPIDsPions/Particles")])
selseqcpDst = SelectionSequence("selseqcpDst",TopSelection=selcpDst) # Selection sequence we will pass to the line builder


noPIDDstar2D0pi = StrippingLine(name+'Line',
                    selection = selcpDst
                    )  


# Append the line to the stream
MyLines = [noPIDDstar2D0pi]
MyStream.appendLines( MyLines )


# Create the stripping configuration which will be passed to the DST writer
from Configurables import ProcStatusCheck
dstStreams  = [ "Charm"]
sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = 'Strip',
                    DSTStreams = dstStreams
                    )

MyStream.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = "noD0MCuts",
                          SelectionSequences = sc.activeStreams()
                          )


# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44434)
##0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number



from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().DataType = "2018"
DaVinci().InputType = "DST"
DaVinci().EvtMax = -1                      # Number of events
DaVinci().PrintFreq = 1000                      # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ eventNodeKiller,checkPV,sc.sequence(),stck,dstWriter.sequence()] )
DaVinci().ProductionType = "Stripping"
                    

DaVinci().Simulation = True
DaVinci().Lumi =  not DaVinci().Simulation


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import DumpFSR, DaVinci
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]
