"""
Stripping filter options for Bc->D0munu 2012 MC (S21r0p2) with no PID requirements
@author Alison Tully
@date 2020-07-28
"""

#use CommonParticlesArchive
stripping='stripping21r0p2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

from Configurables import GaudiSequencer, RawEventJuggler
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstjuggler")
juggler.Sequencer=jseq
juggler.Input="Reco14"
juggler.Output=4.3

#
# Build the streams and stripping object
#
from StrippingSettings.Stripping21r0p2.LineConfigDictionaries_Semileptonic import B2DMuNuX
from StrippingSelections.StrippingSL import StrippingB2DMuNuX
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration

B2DMuNuX['CONFIG']['UseNoPIDsInputs'] = True
B2DMuNuX['CONFIG']['MuonPIDmu']       = -99999
B2DMuNuX['CONFIG']['KaonPIDK']        = -99999
B2DMuNuX['CONFIG']['PionPIDK']        =  99999

MyStream = StrippingStream("Bc2D0MuNu.Strip")
confB2DMuNuX = StrippingB2DMuNuX.B2DMuNuXAllLinesConf("B2DMuNuX", B2DMuNuX['CONFIG'])
lineList= confB2DMuNuX.lines()

MyLines = []
for l in lineList:
    if 'StrippingB2DMuNuX_D0' in l.name():
        MyLines.append(l)
MyStream.appendLines(MyLines)

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()


sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip',
                    AcceptBadEvents=False,
                    BadEventSelection=filterBadEvents)

MyStream.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39162102)


# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

