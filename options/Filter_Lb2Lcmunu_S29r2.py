"""
Stripping filter options for Lb -> Lc(*) mu nu MC 2017 productions
@author Scott Ely
@date 2020-07-02
"""

#use CommonParticlesArchive
stripping='stripping29r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#AllStreams = StrippingStream("B2DMuNuX.Strip")
AllStreams = StrippingStream("Lb2Lcmunu.SafeStripTrig")

linesToAdd = []
for stream in streams:
    if 'Semileptonic' in stream.name():
        for line in stream.lines:
            if 'StrippingB2DMuNuX' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
        'default'               : stripDSTElements(pack=enablePacking)
        }

SelDSTWriterConf = {
        'default'               : stripDSTStreamConf(pack=enablePacking)
        }

#Items that get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                        )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42732920)
# TCK=0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping version

#
# DaVinci Configuration
#
year = "2017"
from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType = year
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import DumpFSR, DaVinci
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.{0}output.txt".format(year)
DaVinci().MoniSequence += [ DumpFSR() ]
