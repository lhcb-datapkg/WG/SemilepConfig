"""
Stripping filter options for Bs -> K mu nu analysis (2016)
@author Adlene Hicheur
@date 2022-09-15
"""

#use CommonParticlesArchive
stripping='stripping28r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

MyStream = StrippingStream("b2KMu.TrigStrip")

linesToFilter = ["StrippingB2XuMuNuB2Phi_Line",
                 "StrippingB2XuMuNuBs2K_NoPIDKMuLine",
                 "StrippingB2XuMuNuBs2K_SSNoPIDKMuLine",
                 "StrippingB2XuMuNuBs2Kstar_Line",	
                 "StrippingB2XuMuNuBs2Kstar_SSLine"]

linesToAdd = []
for stream in streams:
    if 'Semileptonic' in stream.name():
        for line in stream.lines:
            for myLine in linesToFilter:
                if myLine in line.name():
                    line._prescale = 1.0
                    linesToAdd.append(line)
                    
MyStream.appendLines(linesToAdd)

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

MyStream.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)
# TCK=0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping version

#add trigger requirement
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon')",
    HLT1_Code = "(HLT_PASS_RE('Hlt1Track.*Decision') | HLT_PASS_RE('Hlt1.*Muon.*Decision'))",
    HLT2_Code = "(HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2.*SingleMuon.*Decision'))"
            )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().InputType = "DST"
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


