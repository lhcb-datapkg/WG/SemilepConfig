from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from Configurables import FilterDesktop
from Configurables import LoKiSvc


from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci
name = "dsttaunu"

_stdPions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
_pionFilter = FilterDesktop('pionFilter', Code = "(P>2.0*GeV) & (PT > 300 *MeV)"\
                   "&(TRGHOSTPROB < 0.5) & (MIPCHI2DV(PRIMARY)>0)")
PionFilterSel = Selection(name = 'PionFilterSel',
                          Algorithm = _pionFilter,
                          RequiredSelections = [ _stdPions ])

_stdSlowPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_SlowpionFilter = FilterDesktop('SlowpionFilter', Code = "ALL")
SlowPionFilterSel = Selection(name = 'SlowPionFilterSel',
                          Algorithm = _SlowpionFilter,
                          RequiredSelections = [ _stdSlowPions ])
                          


_stdKaons = DataOnDemand(Location = "Phys/StdLooseKaons/Particles")                          
_kaonFilter = FilterDesktop('kaonFilter', Code = "(P>2.0*GeV) & (PT > 300 *MeV)"\
                   "    & (TRGHOSTPROB < 0.5)& (MIPCHI2DV(PRIMARY)>0)")
KaonFilterSel = Selection(name = 'KaonFilterSel',
                          Algorithm = _kaonFilter,
                          RequiredSelections = [ _stdKaons ])


_stdMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
_muonFilter = FilterDesktop('muonFilter', Code = "(TRGHOSTPROB < 0.5) & (P> 3.0*GeV) & (MIPCHI2DV(PRIMARY)>0)")
MuonFilterSel = Selection(name = 'MuonFilterSel',
                          Algorithm = _muonFilter,
                          RequiredSelections = [ _stdMuons ])
                          
                          
_makeD = CombineParticles("makeD_" + name,
                DecayDescriptor = "[D~0 -> K+ pi-]cc",
                CombinationCut = "(ACHILD(PT,1)+ACHILD(PT,2) > 1400 *MeV)",
			    MotherCut = "(SUMTREE( PT,  ISBASIC )> 1400 * MeV) & (VFASPF(VCHI2/VDOF) < 4) " \
                            "& (BPVVDCHI2 > 250) &  (BPVDIRA> 0.9998)"
)
selD = Selection ("SelD",
                     Algorithm = _makeD,
                     RequiredSelections = [KaonFilterSel,PionFilterSel])
                               
_makeB = CombineParticles("SelB",
                            DecayDescriptor = "[B0 ->  D0 mu-]cc",
                            MotherCut = "(VFASPF(VCHI2/VDOF)< 6) & (BPVDIRA> 0.9995)"
                            )
selB = Selection ("Sel"+name,
                     Algorithm = _makeB,
                     RequiredSelections = [selD,MuonFilterSel])



from Configurables import TisTosParticleTagger

_trigfilter = TisTosParticleTagger(name + "Filter")
_trigfilter.TisTosSpecs = {"Hlt2CharmHadD02HH_D02KPiDecision%TOS" : 0}

_trigfilter.ProjectTracksToCalo = False
_trigfilter.CaloClustForCharged = False
_trigfilter.CaloClustForNeutral = False
_trigfilter.TOSFrac = { 4:0.0, 5:0.0 }


selBTrig = Selection ("SelTrig"+name,
                     Algorithm = _trigfilter,
                     RequiredSelections = [selB])
                     

seq = SelectionSequence("Dsttaunu.SafeStripTrig", 
                          TopSelection = selBTrig)

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [seq]
                          )
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )


