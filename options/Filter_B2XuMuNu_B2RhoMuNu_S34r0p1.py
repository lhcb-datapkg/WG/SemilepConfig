"""
Stripping filter options for B -> Xu mu nu MC production (2018)
@author Michel De Cian
@date 2020-03-16
"""

#use CommonParticlesArchive
stripping='stripping34r0p1'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("B2XuMuNu.Strip")

linesToFilter = [#"StrippingB2XuMuNuB2Phi_BadVtxLine",
                 #"StrippingB2XuMuNuB2Phi_Line",
                 #"StrippingB2XuMuNuB2Phi_NoMuTopoLine",
                 #"StrippingB2XuMuNuB2Phi_NoPIDKLine",
                 #"StrippingB2XuMuNuB2Phi_NoPIDMuLine",
                 #"StrippingB2XuMuNuB2Phi_WSLine",
                 #"StrippingB2XuMuNuB2Pi_Line",
                 #"StrippingB2XuMuNuB2Pi_NoMuTopoLine",
                 #"StrippingB2XuMuNuB2Pi_NoPIDMuLine",
                 #"StrippingB2XuMuNuB2Pi_NoPIDPiLine",
                 #"StrippingB2XuMuNuB2Pi_SSLine",
                 #"StrippingB2XuMuNuBs2K_Line",
                 #"StrippingB2XuMuNuBs2K_NoMuTopoLine",
                 #"StrippingB2XuMuNuBs2K_NoPIDKLine",
                 #"StrippingB2XuMuNuBs2K_NoPIDKMuLine",
                 #"StrippingB2XuMuNuBs2K_NoPIDMuLine",
                 #"StrippingB2XuMuNuBs2K_SSLine",	
                 #"StrippingB2XuMuNuBs2K_SSNoPIDKLine",
                 #"StrippingB2XuMuNuBs2K_SSNoPIDKMuLine",
                 #"StrippingB2XuMuNuBs2K_SSNoPIDMuLine",
                 #"StrippingB2XuMuNuBs2Kstar_BadVtxLine",
                 #"StrippingB2XuMuNuBs2Kstar_Line",	
                 #"StrippingB2XuMuNuBs2Kstar_NoMuTopoLine",
                 #"StrippingB2XuMuNuBs2Kstar_NoPIDMuLine",
                 #"StrippingB2XuMuNuBs2Kstar_NoPIDPiLine",
                 #"StrippingB2XuMuNuBs2Kstar_SSLine",
                 #"StrippingB2XuMuNuBu2KshOSMu_SSMuplus_Line",	
                 #"StrippingB2XuMuNuBu2KshSSMu_SSMuminus_Line",
                 #"StrippingB2XuMuNuBu2Omega_BadVtxLine",	
                 #"StrippingB2XuMuNuBu2Omega_Line",	
                 #"StrippingB2XuMuNuBu2Omega_NoMuTopoLine",
                 #"StrippingB2XuMuNuBu2Omega_NoPIDMuLine",
                 #"StrippingB2XuMuNuBu2Omega_NoPIDPiLine",
                 #"StrippingB2XuMuNuBu2Omega_WSLine",
                 "StrippingB2XuMuNuBu2Rho_BadVtxLine",
                 "StrippingB2XuMuNuBu2Rho_Line",	
                 "StrippingB2XuMuNuBu2Rho_NoMuTopoLine",
                 "StrippingB2XuMuNuBu2Rho_NoPIDMuLine",
                 "StrippingB2XuMuNuBu2Rho_NoPIDPiLine",
                 "StrippingB2XuMuNuBu2Rho_WSLine"]

linesToAdd = []
for stream in streams:
    if 'Semileptonic' in stream.name():
        for line in stream.lines:
            for myLine in linesToFilter:
                if myLine in line.name():
                    line._prescale = 1.0
                    linesToAdd.append(line)
                    
AllStreams.appendLines(linesToAdd)

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44103401)
# TCK=0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping version


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType = "2018"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


